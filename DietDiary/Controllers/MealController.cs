﻿using Domain.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList;
using Domain.Entities;

namespace DietDiary.Controllers
{
    [Authorize]
    public class MealController : Controller
    {
        private IUnitOfWork Uow;
        public MealController(IUnitOfWork uow)
        {
            Uow = uow;
        }

        public ActionResult List(int? page, string name = null)
        {
            int pageNumber = page ?? 1;
            var model =
                Uow.MealsRepository.GetMeals()
                .Where(r => name == null || r.Name.StartsWith(name, StringComparison.OrdinalIgnoreCase))
                .Select(r => new Meal
                {
                    MealId = r.MealId,
                    Name = r.Name,
                    Protein = r.Protein,
                    Carbohydrates = r.Carbohydrates,
                    Fat = r.Fat,
                    Calories = r.Calories
                }).ToPagedList(pageNumber, 50);

            var et = Uow.EatenMealsRepository.GetEatenMeals(User.Identity.Name, null);
            List<Meal> meals = new List<Meal>();
            foreach (var item in et)
            {
                meals.Add(item.Meal);
            }
            meals.Reverse();
            meals = meals.GroupBy(o => o.MealId).Select(o => o.FirstOrDefault()).ToList();
            ViewBag.LastAddedEatenMeal = meals.Take(10);

            if (Request.IsAjaxRequest())
            {
                return PartialView("_Meals", model);
            }
            return View(model);
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Meal model)
        {
            if (ModelState.IsValid)
            {
                Uow.MealsRepository.SaveMeal(model);
                Uow.Commit();
                TempData["message"] = string.Format("Dodano do bazy produkt o nazwie {0}", model.Name);
                return RedirectToAction("List");
            }

            else
            {
                return View(model);
            }

        }

        public ViewResult Edit(int id)
        {
            var model = Uow.MealsRepository.GetMeals().Where(x => x.MealId.Equals(id)).SingleOrDefault();
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(Meal model)
        {
            if (ModelState.IsValid)
            {
                Uow.MealsRepository.SaveMeal(model);
                Uow.Commit();
                TempData["message"] = string.Format("Edycja produktu o nazwie {0} zakończona powodzeniem.", model.Name);
                return RedirectToAction("List");
            }

            else
            {
                return View(model);
            }
        }

    }
}
