﻿using DietDiary.Models;
using Domain.Abstract;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using WebMatrix.WebData;

namespace DietDiary.Controllers
{
    [Authorize]
    public class HomeController : Controller
    {
        private IUnitOfWork Uow = null;
        public HomeController(IUnitOfWork uow)
        {
            Uow = uow;
        }

        public ActionResult Index(DateTime? dt)
        {
            ViewModel vm = new ViewModel();
            var today = dt ?? DateTime.Today;
            vm.ProfileModel.User = Uow.UsersRepository.GetUser(User.Identity.Name);
            var eatenMeal = Uow.EatenMealsRepository.GetEatenMeals(User.Identity.Name, today);
            vm.EatenMeals.Add(Uow.MealTypesRepository.GetMealType(1), eatenMeal.Where(o => o.MealType.Id == 1));
            vm.EatenMeals.Add(Uow.MealTypesRepository.GetMealType(2), eatenMeal.Where(o => o.MealType.Id == 2));
            vm.EatenMeals.Add(Uow.MealTypesRepository.GetMealType(3), eatenMeal.Where(o => o.MealType.Id == 3));
            vm.EatenMeals.Add(Uow.MealTypesRepository.GetMealType(4), eatenMeal.Where(o => o.MealType.Id == 4));
            if (Request.IsAjaxRequest())
            {
                ViewBag.TodayDate = today.ToShortDateString();
                return PartialView("_EatenMeals", vm);
            }
            ViewBag.TodayDate = today.ToShortDateString();
            return View(vm);
            
        }

    }
}
