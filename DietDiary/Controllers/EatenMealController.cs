﻿using DietDiary.Models;
using Domain.Abstract;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DietDiary.Controllers
{
    [Authorize]
    public class EatenMealController : Controller
    {
        private IUnitOfWork Uow;
        public EatenMealController(IUnitOfWork uow)
        {
            Uow = uow;
        }

        public ActionResult CreateEatenMeal(int id)
        {
            var meal = Uow.MealsRepository.GetMeal(id);
            List<MealType> mealTypes = new List<MealType>();
            mealTypes = Uow.MealTypesRepository.GetMealTypes();
            EatenMealVM model = new EatenMealVM()
            {
                MealId = meal.MealId,
                MealName = meal.Name,
                MealTypeList = new SelectList(mealTypes, "Id", "Name")
            };
            return View(model);
        }

        [HttpPost]
        public ActionResult CreateEatenMeal(EatenMealVM model)
        {
            if (ModelState.IsValid)
            {
                var meal = Uow.MealsRepository.GetMeal(model.MealId);
                EatenMeal eatenMeal = new EatenMeal()
                {
                    Meal = meal,
                    Serving = model.Serving,
                    Username = User.Identity.Name,
                    MealType = Uow.MealTypesRepository.GetMealType(model.MealTypeID),
                    Date = model.Date
                };
                Uow.EatenMealsRepository.AddEatenMeal(eatenMeal);
                Uow.Commit();
                return RedirectToAction("Index", "Home");
            }
            else
            {
                List<MealType> mealTypes = new List<MealType>();
                mealTypes = Uow.MealTypesRepository.GetMealTypes();
                model.MealTypeList = new SelectList(mealTypes, "Id", "Name");
                return View(model);
            }
        }

        public ActionResult Delete(int id)
        {
            Uow.EatenMealsRepository.Delete(id);
            Uow.Commit();
            return RedirectToAction("Index", "Home");
        }
    }
}
