﻿using DietDiary.Models;
using Domain.Abstract;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebMatrix.WebData;

namespace DietDiary.Controllers
{
    public class AccountController : Controller
    {
        private IUnitOfWork Uow;
        public AccountController(IUnitOfWork uow)
        {
            Uow = uow;
        }

        public ActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Register(RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                WebSecurity.CreateUserAndAccount(model.Name, model.Password, propertyValues: new
                {
                    Activity = 0,
                    Goal = 1
                });
                WebSecurity.Login(model.Name, model.Password);
                return RedirectToAction("Index", "Home");
            }
            return View();
        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(LoginModel model)
        {
            if (ModelState.IsValid && WebSecurity.Login(model.Name, model.Password))
            {
                WebSecurity.Login(model.Name, model.Password);
                return RedirectToAction("Index", "Home");
            }
            else
                TempData["message"] = string.Format("Zła nazwa użytkownika lub hasło.");
            return View();
        }

        public ActionResult LogOff()
        {
            WebSecurity.Logout();
            return Redirect(Url.Action("Login"));
        }

        public ActionResult EditProfile()
        {
            var model = Uow.UsersRepository.GetUser(User.Identity.Name);
            return View(model);
        }
        [HttpPost]
        public ActionResult EditProfile(User model)
        {
            if (ModelState.IsValid)
            {
                Uow.UsersRepository.SaveUser(model);
                Uow.Commit();
                return RedirectToAction("Index", "Home");
            }
            else
                return View(model);
        }
    }
}
