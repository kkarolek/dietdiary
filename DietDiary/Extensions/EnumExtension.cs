﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DietDiary.Extensions
{
    public static class EnumExtension
    {
        public static SelectList ToSelectList<TEnum>(this TEnum enumObj)
        {
            var values = (from TEnum e in Enum.GetValues(typeof(TEnum))
                          select new { ID = e, Name = (e as Enum).GetDescriptionString() }).ToList();

            return new SelectList(values, "Id", "Name", enumObj);
        }

        public static string GetDescriptionString(this Enum val)
        {
            try
            {
                var attributes = (DescriptionAttribute[])val.GetType().GetField(val.ToString()).GetCustomAttributes(typeof(DescriptionAttribute), false);

                return attributes.Length > 0 ? attributes[0].Description : val.ToString();
            }
            catch (Exception)
            {
                return val.ToString();
            }
        }
    }
}