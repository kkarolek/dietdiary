﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Domain.Entities;

namespace DietDiary.Models
{

    public class RegisterModel
    {
        [Required(ErrorMessage = "Proszę podać adres e-mail.")]
        [RegularExpression(".+\\@.+\\..+", ErrorMessage = "Proszę podać prawidłowy adres e-mail.")]
        public string Email { get; set; }
        [Required(ErrorMessage = "Proszę podać nazwę użytkownika.")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Proszę podać hasło.")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
        [Required(ErrorMessage = "Proszę ponownie podać hasło.")]
        [Compare("Password", ErrorMessage = "Hasła nie są takie same.")]
        [DataType(DataType.Password)]
        public string ConfirmPassword { get; set; }
    }

    public class LoginModel
    {
        [Required(ErrorMessage = "Proszę podać nazwę użytkownika.")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Proszę podać hasło.")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }

    public class ProfileModel
    {
        public User User { get; set; }
        public double CalculateCaloricNeeds()
        {
            if (CheckData())
            {
                double dailyCaloricNeeds = 0;
                if (User.IsFemale == true)
                {
                    dailyCaloricNeeds = 655.0955 + (9.5634 * User.Weight.GetValueOrDefault())
                                        + (1.8496 * User.Height.GetValueOrDefault()) - (4.6756 * User.Age.GetValueOrDefault());
                }
                else
                {
                    dailyCaloricNeeds = 66.473 + (13.7516 * User.Weight.GetValueOrDefault()) +
                                      (5.0033 * User.Height.GetValueOrDefault()) - (6.7550 * User.Age.GetValueOrDefault());
                }
                switch (User.Activity)
                {
                    case User.ActivityLevel.NoExercise: dailyCaloricNeeds *= 1.2;
                        break;
                    case User.ActivityLevel.LightExercise: dailyCaloricNeeds *= 1.375;
                        break;
                    case User.ActivityLevel.ModerateExercise: dailyCaloricNeeds *= 1.55;
                        break;
                    case User.ActivityLevel.HeavyExercise: dailyCaloricNeeds *= 1.725;
                        break;
                    case User.ActivityLevel.VeryHeavyExercise: dailyCaloricNeeds *= 1.9;
                        break;
                    default:
                        break;
                }
                switch (User.Goal)
                {
                    case User.DietGoal.Lose: dailyCaloricNeeds -= 300;
                        break;
                    case User.DietGoal.Gain: dailyCaloricNeeds += 300;
                        break;
                    default:
                        break;
                }
                return Math.Round(dailyCaloricNeeds, 0);
            }
            else
            {
                return 0;
            }

        }
        public bool CheckData()
        {
            if (User.Age != null && User.Height != null && User.Weight != null && User.IsFemale != null)
                return true;
            else
                return false;
        }
    }
}