﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DietDiary.Models
{
    public class ViewModel
    {
        public ViewModel()
        {
            ProfileModel = new ProfileModel();
            EatenMeals = new Dictionary<MealType, IEnumerable<EatenMeal>>();
        }
        public ProfileModel ProfileModel { get; set; }
        public IDictionary<MealType, IEnumerable<EatenMeal>> EatenMeals { get; set; }
        public double CaloriesConsumed()
        {
            double caloriesConsumed = 0;
            foreach (var kvp in EatenMeals)
            {
                foreach (var eaten in kvp.Value)
                {
                    caloriesConsumed += CalcEatenMacronutrientsAndCalories(eaten.Serving, eaten.Meal.Calories);
                }
            }
            return caloriesConsumed;
        }
        public double ServingConsumed()
        {
            double servingConsumed = 0;
            foreach (var kvp in EatenMeals)
            {
                servingConsumed += kvp.Value.Sum(o => o.Serving);
            }
            return servingConsumed;
        }
        public double ProteinConsumed()
        {
            double proteinConsumed = 0;
            foreach (var kvp in EatenMeals)
            {
                foreach (var eaten in kvp.Value)
                {
                    proteinConsumed += CalcEatenMacronutrientsAndCalories(eaten.Serving, eaten.Meal.Protein);
                }
            }
            return proteinConsumed;
        }
        public double CarbohydratesConsumed()
        {
            double carbohydratesConsumed = 0;
            foreach (var kvp in EatenMeals)
            {
                foreach (var eaten in kvp.Value)
                {
                    carbohydratesConsumed += CalcEatenMacronutrientsAndCalories(eaten.Serving, eaten.Meal.Carbohydrates);
                }
            }
            return carbohydratesConsumed;
        }
        public double FatConsumed()
        {
            double fatConsumed = 0;
            foreach (var kvp in EatenMeals)
            {
                foreach (var eaten in kvp.Value)
                {
                    fatConsumed += CalcEatenMacronutrientsAndCalories(eaten.Serving, eaten.Meal.Fat);
                }
            }
            return fatConsumed;
        }

        public double CalcEatenMacronutrientsAndCalories(double serving, double macro)
        {
            return Math.Round(macro * serving / 100, 2);
        }
    }
}