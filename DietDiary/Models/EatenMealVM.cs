﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DietDiary.Models
{
    public class EatenMealVM
    {
        public int MealId { get; set; }
        [Display(Name = "Nazwa posiłku")]
        public string MealName { get; set; }
        [Display(Name = "Typ posiłku")]
        [Required(ErrorMessage = "Proszę wybrać typ posiłku")]
        public int MealTypeID { get; set; }
        [Display(Name = "Porcja (g)")]
        [Required(ErrorMessage = "Proszę podać ilość spożywanej porcji.")]
        [Range(0.00, double.MaxValue, ErrorMessage = "Proszę podać dodatnią ilość.")]
        public double Serving { get; set; }
        [Display(Name = "Data spożycia")]
        [Required(ErrorMessage = "Proszę podać datę spożycia")]
        [DataType(DataType.Date)]
        public DateTime Date { get; set; }
        public SelectList MealTypeList { get; set; }
    }
}