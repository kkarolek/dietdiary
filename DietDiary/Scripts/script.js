﻿var chart;

$(document).on("click", ".pagedList a", function () {
    var $a = $(this);
    $.ajax({
        url: $a.attr("href"),
        data: { name: $("#searchInput").val() },
        type: 'GET'
    })
    .done(function (data) {
        $("#meals").replaceWith(data);
    });
    return false;
});

$(document).on("click", "#searchButton", function () {
    var $a = $("#searchInput").val();
    $.ajax({
        type: 'GET',
        url: '/Meal/List',
        data: { name: $a }
    })
    .done(function (data) {
        $("#meals").replaceWith(data);
    });
    return false;
});

$(document).on("click", ".change-day", function () {
    var date = $(".diary-date").text();
    var fullDate = new Date(date);
    var $id = $(this).attr('id');
    if ($id == "next-day") {
        fullDate.setDate(fullDate.getDate() + 1);
    }
    else {
        fullDate.setDate(fullDate.getDate() - 1);
    }
    var twoDigitMonth = ((fullDate.getMonth() + 1) < 10 ? '0' : '') + (fullDate.getMonth() + 1);
    var twoDigitDay = (fullDate.getDate() < 10 ? '0' : '') + fullDate.getDate();
    fullDate = fullDate.getFullYear() + "-" + twoDigitMonth + "-" + twoDigitDay;

    $.ajax({
        type: 'GET',
        url: '/Home/Index',
        data: { dt: fullDate }
    })
    .done(function (data) {
        $("#eatenMeals").replaceWith(data);
        DrawChart();
    });
    return false;
})


$(document).on("focus", "#date-picker", function () {
    chart;
    $(this).datepicker({
        format: "yyyy-mm-dd",
        language: "pl",
        todayHighlight: true
    }).on("changeDate", function () {
        $('#date-picker').datepicker('hide');
        var fullDate = new Date($('#date-picker').datepicker('getDate'));
        var twoDigitMonth = ((fullDate.getMonth() + 1) < 10 ? '0' : '') + (fullDate.getMonth() + 1);
        var twoDigitDay = (fullDate.getDate() < 10 ? '0' : '') + fullDate.getDate();
        fullDate = fullDate.getFullYear() + "-" + twoDigitMonth + "-" + twoDigitDay;

        $.ajax({
            type: 'GET',
            url: '/Home/Index',
            data: { dt: fullDate }
        })
        .done(function (data) {
            $("#eatenMeals").replaceWith(data);
            DrawChart();
        });
        return false;
    });
});

$(document).ready(function () {
    $("#date-eaten").datepicker({
    })
})

$(document).ready(function () {
    var calories = parseFloat($("#remaining-calories").text())
    if (calories < 0) {
        $("#remaining-calories").parent().html('Przekroczono!');
    }
})


// delete EatenMeal
$(document).on("click", ".fa-remove", function () {
    $.ajax({
        type: 'GET',
        url: '/EatenMeal/Delete',
        data: { id: $(this).attr('id') }
    })
    .done(function (data) {
        $("#eatenMeals").replaceWith(data);
        DrawChart();
    });
    return false;
});

$(document).ready(function () {
    DrawChart();
})

//pie chart
function DrawChart() {
    var fat = $("#fat").text();
    var carbo = $("#carbo").text();
    var protein = $("#protein").text();
    if (fat != 0 && carbo != 0 && protein != 0)
    {
        $('#pieChart').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false
            },
            title: null,
            tooltip: {
                pointFormat: '<b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        format: '<b>{point.name}</b><br/>{point.percentage:.1f} %',
                        style: {
                            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                        }
                    }
                }
            },
            series: [{
                type: 'pie',
                name: 'Makroskładniki',
                data: [
                    ['Białko', parseFloat(protein)],
                    ['Węglowodany', parseFloat(carbo)],
                    ['Tłuszcze', parseFloat(fat)]
                ]
            }]
        });
    }
};

