# DietDiary #

### What is it? ###

DietDiary is a web application which is purposed for people who want control calories consumed by them. User can add meals to database and then supplements own food diary. A fully supplemented profile allows calculate caloric needs, thereby user can achieve better results.

### Utilised technologies ###

* C#
* ASP.NET MVC 4
* ENTITY FRAMEWORK
* HTML
* CSS
* AJAX
* BOOTSTRAP

### Screenshots ###

* [Main page](http://i.imgur.com/btMVgXx.png)
* [List of meals](http://i.imgur.com/sbShdr3.png)
* [Profile page](http://i.imgur.com/9i0retM.png)