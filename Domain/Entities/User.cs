﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Entities
{
    public class User
    {
        public enum ActivityLevel
        {
            [Description("Mała, bez ćwiczeń.")]
            NoExercise,
            [Description("Lekkie ćwczenia (1-3 dni w tygodniu).")]
            LightExercise,
            [Description("Umiarkowane ćwiczenia (3-5 dni w tygodniu).")]
            ModerateExercise,
            [Description("Ciężkie ćwiczenia (6-7 dni w tygodniu).")]
            HeavyExercise,
            [Description("Bardzo ciężkie ćwiczenia (dwa razy dziennie, dodatkowe ćwiczenia).")]
            VeryHeavyExercise
        }

        public enum DietGoal
        {
            [Description("Redukcja")]
            Lose,
            [Description("Utrzymanie wagi")]
            Maintain,
            [Description("Zwiększenie masy")]
            Gain
        }

        public int UserId { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public double? DailyCaloric { get; set; }
        public int? Age { get; set; }
        public double? Weight { get; set; }
        public double? Height { get; set; }
        public bool? IsFemale { get; set; }
        public ActivityLevel Activity { get; set; }
        public DietGoal Goal { get; set; }

    }
}
