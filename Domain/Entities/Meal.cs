﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Domain.Entities
{
    public class Meal
    {
        [HiddenInput(DisplayValue = false)]
        public int MealId { get; set; }
        [Required(ErrorMessage = "Proszę podać nazwę posiłku")]
        public string Name { get; set; }
        [Required(ErrorMessage = "Proszę podać ilość białka")]
        [Range(0.00, double.MaxValue, ErrorMessage = "Proszę podać dodatnią ilość.")]
        public double Protein { get; set; }
        [Required(ErrorMessage = "Proszę podać ilość węglowodanów")]
        [Range(0.00, double.MaxValue, ErrorMessage = "Proszę podać dodatnią ilość.")]
        public double Carbohydrates { get; set; }
        [Required(ErrorMessage = "Proszę podać ilość tłuszczy")]
        [Range(0.00, double.MaxValue, ErrorMessage = "Proszę podać dodatnią ilość.")]
        public double Fat { get; set; }
        [Required(ErrorMessage = "Proszę podać ilość kalorii")]
        [Range(0.00, double.MaxValue, ErrorMessage = "Proszę podać dodatnią ilość.")]
        public double Calories { get; set; }
    }
}
