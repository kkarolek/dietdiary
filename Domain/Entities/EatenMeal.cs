﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Domain.Entities
{

    public class EatenMeal
    {
        public int Id { get; set; }
        public double Serving { get; set; }
        public string Username { get; set; }
        public DateTime Date { get; set; }
        public virtual Meal Meal { get; set; }
        public virtual MealType MealType { get; set; }
    }
}
