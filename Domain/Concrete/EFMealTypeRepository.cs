﻿using Domain.Abstract;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Concrete
{
    public class EFMealTypeRepository : IMealTypeRepository
    {
        private EFDbContext context = null;

        public EFMealTypeRepository(EFDbContext _context)
        {
            context = _context;
        }

        public MealType GetMealType(int id)
        {
            return context.MealTypes.Where(o => o.Id == id).Single();
        }

        public List<MealType> GetMealTypes()
        {
            return context.MealTypes.ToList();
        }
    }
}
