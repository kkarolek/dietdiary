﻿using Domain.Abstract;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Concrete
{
    public class EFUserRepository : IUserRepository
    {
        EFDbContext context = null;

        public EFUserRepository(EFDbContext _context)
        {
            context = _context;
        }

        public User GetUser(string name)
        {
            var user = context.Users.Where(x => x.UserName.Equals(name)).SingleOrDefault();
            return user;
        }
        public void SaveUser(User user)
        {
            var dbEntry = context.Users.Find(user.UserId);
            if (dbEntry != null)
            {
                dbEntry.Activity = user.Activity;
                dbEntry.Age = user.Age;
                dbEntry.DailyCaloric = user.DailyCaloric;
                dbEntry.IsFemale = user.IsFemale;
                dbEntry.Height = user.Height;
                dbEntry.FirstName = user.FirstName;
                dbEntry.LastName = user.LastName;
                dbEntry.Weight = user.Weight;
                dbEntry.Goal = user.Goal;
            }
        }
    }
}
