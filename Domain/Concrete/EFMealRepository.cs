﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Domain.Abstract;
using Domain.Entities;

namespace Domain.Concrete
{
    public class EFMealRepository : IMealRepository
    {
        private EFDbContext context = null;

        public EFMealRepository(EFDbContext _context)
        {
            context = _context;
        }

        public IEnumerable<Meal> GetMeals()
        {
            var model = context.Meals.ToList();
            return model;
        }
        public Meal GetMeal(int id)
        {
            var meal = context.Meals.Where(o => o.MealId == id).SingleOrDefault();
            return meal;
        }
        public void SaveMeal(Meal meal)
        {
            if (meal.MealId == 0)
                context.Meals.Add(meal);
            else
            {
                Meal dbEntry = context.Meals.Find(meal.MealId);
                if (dbEntry != null)
                {
                    dbEntry.Name = meal.Name;
                    dbEntry.Protein = meal.Protein;
                    dbEntry.Carbohydrates = meal.Carbohydrates;
                    dbEntry.Fat = meal.Fat;
                    dbEntry.Calories = meal.Calories;
                }
            }
        }
    }
}
