﻿using Domain.Abstract;
using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Concrete
{
    public class EFEatenMealRepository : IEatenMealRepository
    {
        private EFDbContext context = null;
        public EFEatenMealRepository(EFDbContext _context)
        {
            context = _context;
        }

        public IEnumerable<EatenMeal> GetEatenMeals(string userName, DateTime? dt)
        {
            {
                if (dt != null)
                {
                    var eatenMeal = context.EatenMeals.Where(o => o.Username == userName && o.Date == dt).ToList();
                    return eatenMeal;
                }
                else
                {
                    var eatenMeal = context.EatenMeals.Where(o => o.Username == userName);
                    return eatenMeal;
                }

            }
        }
        public void AddEatenMeal(EatenMeal eatenMeal)
        {
            context.Meals.Attach(eatenMeal.Meal);
            context.MealTypes.Attach(eatenMeal.MealType);
            context.EatenMeals.Add(eatenMeal);
        }

        public void Delete(int id)
        {
            var eatenMeal = context.EatenMeals.Where(o => o.Id == id).Single();
            if(eatenMeal != null)
            {
                context.EatenMeals.Remove(eatenMeal);
            }
        }
    }
}
