﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Domain.Entities;

namespace Domain.Concrete
{
    public class EFDbContext : DbContext
    {
        public EFDbContext()
            : base("name=DefaultConnection")
        {

        }
        public DbSet<Meal> Meals { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<EatenMeal> EatenMeals { get; set; }
        public DbSet<MealType> MealTypes { get; set; }
    }
}
