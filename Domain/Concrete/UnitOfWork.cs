﻿using Domain.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Concrete
{
    public class UnitOfWork : IUnitOfWork, IDisposable
    {
        private EFDbContext context = null;

        public UnitOfWork()
        {
            context = new EFDbContext();
        }

        public IEatenMealRepository EatenMealsRepository
        {
            get { return new EFEatenMealRepository(context); }
        }

        public IMealRepository MealsRepository
        {
            get { return new EFMealRepository(context); }
        }

        public IMealTypeRepository MealTypesRepository
        {
            get { return new EFMealTypeRepository(context); }
        }

        public IUserRepository UsersRepository
        {
            get { return new EFUserRepository(context); }
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);

        }

        public void Commit()
        {
            context.SaveChanges();
        }
    } 
}
