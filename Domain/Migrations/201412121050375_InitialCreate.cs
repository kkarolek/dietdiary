namespace Domain.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            //CreateTable(
            //    "dbo.EatenMeals",
            //    c => new
            //        {
            //            Id = c.Int(nullable: false, identity: true),
            //            Serving = c.Double(nullable: false),
            //            Username = c.String(),
            //            Date = c.DateTime(nullable: false),
            //            Meal_MealId = c.Int(),
            //            MealType_Id = c.Int(),
            //        })
            //    .PrimaryKey(t => t.Id)
            //    .ForeignKey("dbo.Meals", t => t.Meal_MealId)
            //    .ForeignKey("dbo.MealTypes", t => t.MealType_Id)
            //    .Index(t => t.Meal_MealId)
            //    .Index(t => t.MealType_Id);
            
            //CreateTable(
            //    "dbo.Meals",
            //    c => new
            //        {
            //            MealId = c.Int(nullable: false, identity: true),
            //            Name = c.String(nullable: false),
            //            Protein = c.Double(nullable: false),
            //            Carbohydrates = c.Double(nullable: false),
            //            Fat = c.Double(nullable: false),
            //            Calories = c.Double(nullable: false),
            //        })
            //    .PrimaryKey(t => t.MealId);
            
            //CreateTable(
            //    "dbo.MealTypes",
            //    c => new
            //        {
            //            Id = c.Int(nullable: false, identity: true),
            //            Name = c.String(),
            //        })
            //    .PrimaryKey(t => t.Id);
            
            //CreateTable(
            //    "dbo.Users",
            //    c => new
            //        {
            //            UserId = c.Int(nullable: false, identity: true),
            //            UserName = c.String(),
            //            FirstName = c.String(),
            //            LastName = c.String(),
            //            DailyCaloric = c.Double(),
            //            Age = c.Int(),
            //            Weight = c.Double(),
            //            Height = c.Double(),
            //            IsFemale = c.Boolean(),
            //            Activity = c.Int(nullable: false),
            //            Goal = c.Int(nullable: false),
            //        })
            //    .PrimaryKey(t => t.UserId);
            
        }
        
        public override void Down()
        {
            //DropForeignKey("dbo.EatenMeals", "MealType_Id", "dbo.MealTypes");
            //DropForeignKey("dbo.EatenMeals", "Meal_MealId", "dbo.Meals");
            //DropIndex("dbo.EatenMeals", new[] { "MealType_Id" });
            //DropIndex("dbo.EatenMeals", new[] { "Meal_MealId" });
            //DropTable("dbo.Users");
            //DropTable("dbo.MealTypes");
            //DropTable("dbo.Meals");
            //DropTable("dbo.EatenMeals");
        }
    }
}
