﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Abstract
{
    public interface IUnitOfWork
    {
        void Commit();
        IEatenMealRepository EatenMealsRepository { get; }
        IMealRepository MealsRepository { get; }
        IMealTypeRepository MealTypesRepository { get; }
        IUserRepository UsersRepository { get; }
    }
}
