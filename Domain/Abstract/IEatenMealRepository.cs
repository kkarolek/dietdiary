﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Abstract
{
    public interface IEatenMealRepository
    {
        IEnumerable<EatenMeal> GetEatenMeals(string userName, DateTime? dt);
        void AddEatenMeal(EatenMeal eatenMeal);
        void Delete(int id);

    }
}
