﻿using Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Abstract
{
    public interface IMealRepository
    {
        IEnumerable<Meal> GetMeals();
        Meal GetMeal(int id);
        void SaveMeal(Meal meal);
    }
}
